from django.http import HttpResponse
from django.shortcuts import render
import math
import json
import requests
import toolforge

def index(request):
    return(render(request, 'Microtask1/home.html',{}))     

def main(request):
    if(request.method=='POST'):
        usr = request.POST['username']  #Here, we get the username fetched from html page
        URL = "https://en.wikipedia.org/w/api.php?action=query&format=json&list=usercontribs&ucuser="
        URL = URL + usr
        r = requests.get(URL)  #Recieved data in json-format
        if(r.status_code != 200):
            return(render(request,'Microtask1/index.html',{"error":'HTTP response ' + str(r.status_code)}))
        else:
            lst = json.loads(r.text)
            try:    
                if(lst['error']):
                    return(render(request,'Microtask1/index.html',{"error":lst['error']['info']}))
            except:   
                sub_lst = lst['query']['usercontribs'] #Contribution data extracted
                if(len(sub_lst)==0):
                    return(render(request,'Microtask1/index.html',{"error":"No Edits found"}))
            return(render(request,'Microtask1/index.html',{"contribution":sub_lst, "username":usr}))  #Here I send contribution for display on index.html page
    else:
        return(render(request,'Microtask1/index.html',{}))


def main2(request):
    if(request.method=='POST'):
        flag = 1
        usr = request.POST['username']
        SqlQuery1 = "SELECT user_editcount FROM user WHERE user_name='" + usr + "';"
        SqlQuery2 = "SELECT ss_total_edits FROM site_stats"
        #SqlQuery3 = "select rank from (select user_name, @curRank := @curRank + 1 as rank from user p, ( select @curRank := 0 ) q order by user_editcount DESC) q where user_name='"+usr+"';"
        SqlQuery3 = "SELECT 1+(SELECT count(*) from user a WHERE a.user_editcount > b.user_editcount) as RNK FROM user b WHERE user_name='"+usr+"';"
        SqlQuery4 = "select count(*) from user;"
        conn = toolforge.connect('kawiki_p')
        with conn.cursor() as curr:
            s1 = curr.execute(SqlQuery1)
            useredits_tup = curr.fetchall()
            s2 = curr.execute(SqlQuery2)
            totaledits_tup =curr.fetchall()
            s3 = curr.execute(SqlQuery3)
            userrank_tup = curr.fetchall()
            s4 = curr.execute(SqlQuery4)
            totalusers_tup = curr.fetchall()

        conn.close()
        try:    
            useredits = useredits_tup[0][0]
            totaledits = totaledits_tup[0][0]
            userrank = userrank_tup[0][0]
            totalusers = totalusers_tup[0][0]
        except:
            flag = 0 

        if(flag):    
            percentile = 100-((userrank*100)/(totalusers+1))
            rating = math.ceil((userrank/totalusers)*100)
            userrating = "User lies in top "+str(rating) + "%"
            return(render(request, 'Microtask1/task2.html',{'edits':useredits, 'name':usr, 'total_edits':totaledits, 'rank':userrank, 'total_users':totalusers, 'Percentile':percentile, 'user_rating':userrating}))
        
        return(render(request, 'Microtask1/task2.html',{"false_query":1}))
    else:
        return(render(request, 'Microtask1/task2.html',{}))
